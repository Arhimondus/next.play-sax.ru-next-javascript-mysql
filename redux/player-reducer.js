const initialState = null;

export default function(state = initialState, action) {
	switch (action.type) {
		case 'set/song':
			const { song, title } = action.payload;
			return { song, title };
		case 'close/player':
			return null;
		default:
			return state;
	}
}