import { combineReducers } from 'redux';

import heroReducer from './hero-reducer';
import playerReducer from './player-reducer';

const allReducers = combineReducers({
	hero: heroReducer,
	player: playerReducer,
});

export default allReducers;