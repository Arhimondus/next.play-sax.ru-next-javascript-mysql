const initialState = null;

export default function (state=initialState, action) {
    switch (action.type) {
        case 'set/hero':
            return action.payload;
        default:
            return state;
    }
}