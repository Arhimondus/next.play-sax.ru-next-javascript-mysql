import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import './[content].module.scss';
import axios from 'lib/axios';
import { faInfoCircle, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Slider from '@farbenmeer/react-spring-slider';
import { useRouter } from 'next/router';
import Link from 'next/link';

export async function getServerSideProps({ req, res, query: { note } }) {
	const { data: contents } = await axios.get(`/api/note-contents/`, {
		params: { note },
	});

	const { data } = await axios.get(`/api/note-by-link/`, {
		params: { note },
	});

	return {
		props: { contents, note: data },
	};
}

function NoteContent({ contents }) {
	const router = useRouter();
	const dispatch = useDispatch();

	const { note, content } = router.query;

	console.log(router);
	console.log('note', note, 'content', content);

	const [currentContent, setCurrentContent] = useState(contents.find(it => it.url === content));
	
	useEffect(() => {
		dispatch({ type: 'set/hero', payload: 'main' });
		setCurrentContent(contents.find(it => it.url === content));
	}, [contents, content]);

	// const instruments = contents.map(content => content.ins);
	// console.log('currentContent', currentContent, 'contentsLoading', contentsLoading);

	return <div className="main-content">
		<div className="main-content__block">
			<nav className="main-content__nav">
				<ul>
					<li className="checked">
						<Link href={'/note/' + currentContent.note_url}><a className="main-content__link"><FontAwesomeIcon icon={faInfoCircle} className=""/></a></Link>
					</li>
					{contents.map(content =>
						<li key={content.id}>
							<a href={'/note' + content.full_url} className={['main-content__link', content === currentContent ? '--active' : ''].join(' ')} onClick={evt => {
								if(!evt.ctrlKey) {
									evt.preventDefault();
									setCurrentContent(content);
								}
							}}>{content.title}</a>	
						</li>,
					)}
				</ul>
			</nav>
			<h1>{currentContent.composers} - {currentContent.note_title} ({currentContent.title}) </h1>
			<div className="main-content__content">
				<div className="main-content__slider">
					Всего страниц: {currentContent.pages_count}
					{currentContent.pages_count > 0 &&
					<Slider
						activeIndex={0}
						hasBullets
						hasArrows={1}
						slidesAtOnce={2}
					>
						{new Array(currentContent.pages_count).fill(null).map((_, index) => {
							return <div key={index}><img
								src={`/contents/content${currentContent.id}-${index + 1}.jpg`}/></div>;
						})}
					</Slider>}
				</div>
				<ul>
					{currentContent.youtube_link &&
					<li>
						<a className="main-content__link" href={currentContent.youtube_link} target="_blank"
						   rel="noopener">Ссылка на ютуб</a>
					</li>}
					{currentContent.download_link &&
					<li>
						<a className="main-content__link" href={currentContent.download_link} target="_blank"
						   rel="noopener">Скачать</a>
					</li>}
					{currentContent.audio_file &&
					<li>
						<div className="main-content__link" onClick={() => {
							dispatch({
								type: 'set/song',
								payload: {
									song: `/audio/${currentContent.audio_file}`,
									title: `${currentContent.composers} - ${currentContent.note_title}`,
								},
							});
						}}>Проиграть
						</div>
					</li>}
				</ul>
			</div>
		</div>
	</div>;
}

export default NoteContent;