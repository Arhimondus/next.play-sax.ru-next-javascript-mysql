// next.config.js
module.exports = {
	webpack(config, options) {
		config.module.rules[1].oneOf.forEach((moduleLoader, i) => {
			Array.isArray(moduleLoader.use) &&
				moduleLoader.use.forEach((l) => {
					if (l.loader.includes('css-loader') && !l.loader.includes('postcss-loader')) {
						delete l.options.modules.getLocalIdent;
						l.options.modules = {
							...l.options.modules,
							// Your custom css-modules options below.
							localIdentName: '[local]',
			exportLocalsConvention: 'camelCaseOnly'
						};
					}
				});
		});
		return config;
	},
	async rewrites() {
		return [
			{
				source: `/${encodeURIComponent('популярные-мелодии')}/${encodeURIComponent('альт')}`,
				destination: `/notes/${encodeURIComponent('популярные-мелодии')}/${encodeURIComponent('альт')}`,
			},
			{
				source: `/${encodeURIComponent('популярные-мелодии')}/${encodeURIComponent('сопрано')}`,
				destination: `/notes/${encodeURIComponent('популярные-мелодии')}/${encodeURIComponent('сопрано')}`,
			},
			{
				source: `/${encodeURIComponent('классические-произведения')}/${encodeURIComponent('альт')}`,
				destination: `/notes/${encodeURIComponent('классические-произведения')}/${encodeURIComponent('альт')}`,
			},
			{
				source: `/${encodeURIComponent('классические-произведения')}/${encodeURIComponent('сопрано')}`,
				destination: `/notes/${encodeURIComponent('классические-произведения')}/${encodeURIComponent('сопрано')}`,
			},
			{
				source: '/note/:note/:content',
				destination: '/note/:note?content=:content',
			},
		];
	},
};