import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import './[instrument].module.scss';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';
import axios from 'lib/axios';
import { faInfoCircle, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Slider from '@farbenmeer/react-spring-slider';
import Link from 'next/link';
import Head from 'next/head';

export async function getServerSideProps({ req, res, query: { genre, instrument } }) {
	const defaultPageNumber = 1;
	const defaultPageSize = 20;
	const defaultSortType = 'composer';

	const { data: header } = await axios.get('/api/notes-header', { params: { genre, instrument } });
	const { data: composers } = await axios.get(`/api/composers`, { params: { genre, instrument }});
	const { data: levels } = await axios.get('/api/levels', { params: { genre, instrument }});

	const { data: defaultContents } = await axios.get(`/api/all-contents`, {
		params: { genre, instrument, pageNumber: defaultPageNumber, pageSize: defaultPageSize, composerIds: [], levelIds: [], sortType: defaultSortType },
	});

	// console.log('contents', defaultContents);

	return {
		props: { header, defaultPageNumber, defaultPageSize, defaultContents, composers, levels },
	};
}

export default function Notes({ header, defaultPageNumber, defaultPageSize, defaultContents, composers, levels }) {
	const router = useRouter();
	const { genre, instrument } = router.query;

	// console.log('gi', genre, instrument);

	const [contents, setContents] = useState(group(defaultContents));
	const [pageNumber, setPageNumber] = useState(defaultPageNumber);
	const [pageSize, setPageSize] = useState(defaultPageSize);

	const [composerIds, setComposerIds] = useState([]);
	const [levelIds, setLevelIds] = useState([]);
	const [composerSort, setComposerSort] = useState([]);

	const SortTypes = {
		composer: ['composer', 'Композитору'],
		note: ['note', 'Нотам'],
	};

	const [sortType, setSortType] = useState(SortTypes.composer);

	// useEffect(() => {
	// 	applyFilter();
	// }, [composerIds, levelIds, pageNumber, sortType]);

	function group(contents) {
		const total = {};
		// console.log('group', contents);
		contents.forEach(content => {
			const noteId = content.note_id;
			if (!total[noteId]) {
				total[noteId] = [];
			}
			total[noteId].push(content);
		});
		return total;
	}

	return <div className="main-notes">
		<Head>
			<title>123</title>
			<link rel="icon" href="/favicon.ico" />
			<meta description="ЭТО САЙТ КОВАЛЬСКОГО!"/>
		</Head>
		<div className="main-notes__block">
			<div className="main-notes__content">
				<div><h1>{header}</h1></div>
				<table class="kovalsky-table">
					<tbody>
					{Object.entries(contents).map(([note_id, contents]) => { 
						const [f, s] = contents;
						return <tr className="notes-content" key={`note-${note_id}`}>
							<td className="notes-content__titleColumn">{f.composers} - {f.note_title}</td>
							<td className="notes-content__linksColumn">
								<Link className="notes-content__link" href={'/note' + f.url}><a>НОТЫ</a></Link>
								{s && <Link className="notes-content__link" href={'/note' + s.url}><a>МИНУС</a></Link>}
							</td>
						</tr>;
					})}
					{/*<tr>*/}
					{/*	<td colSpan={3}>*/}
					{/*		<div>Страница {pageNumber}</div>*/}
					{/*		<button onClick={() => setPageNumber(pageNumber => pageNumber + 1)}>Следующая страница</button>*/}
					{/*	</td>*/}
					{/*</tr>*/}
					</tbody>
				</table>
			</div>

			<div className="notes-sidebar">
				<ul>
					{/*<li>*/}
					{/*	<div ><input placeholder="Поиск..."/></div>*/}
					{/*</li>*/}
					<li>
						<ul>
							<li><p>По композитору</p></li>
							{composers.map(composer => <li>
								<label>
									<input
										style={{ display: 'inline-block' }}
										value={composer.id}
										type="checkbox"
										onChange={({ target: { checked, value } }) => {
											if (checked) {
												setComposerIds([...composerIds, +value]);
											} else {
												setComposerIds(composerIds.filter(composerId => composerId !== +value));
											}
										}}/>{composer.name}
								</label>
							</li>)}
						</ul>
					</li>
					{/*Здесь должна быть сортировка?*/}
					{/*<li>*/}
					{/*	<div>*/}
					{/*		<button onClick={() => applySort('name')}>Применить</button>*/}
					{/*	</div>*/}
					{/*</li>*/}
					<li>
						<ul>
							<li><p>По уровню</p></li>
							{levels.map(level => <li>
								<label>
									<input
										style={{ display: 'inline-block' }}
										value={level.id} type="checkbox"
										onChange={({ target: { checked, value } }) => {
											if (checked) {
												setLevelIds([...levelIds, +value]);
											} else {
												setLevelIds(levelIds.filter(levelId => levelId !== +value));
											}
										}}/>{level.title}
								</label>
							</li>)}
						</ul>
					</li>
					{/*<li>*/}
					{/*	<ul>*/}
					{/*		{JSON.stringify(sortType)}*/}
					{/*		{Object.keys(SortTypes).map(key => <li>*/}
					{/*			<label>*/}
					{/*				<input*/}
					{/*					name="sortType"*/}
					{/*					style={{ display: 'inline-block' }}*/}
					{/*					value={key} type="checkbox"*/}
					{/*					checked={SortTypes[key] === sortType}*/}
					{/*					type="radio"*/}
					{/*					onChange={({ target: { checked, value } }) => {*/}
					{/*						if (checked) {*/}
					{/*							setSortType(SortTypes[value]);*/}
					{/*						}*/}
					{/*					}}/>{SortTypes[key][1]}*/}
					{/*			</label>*/}
					{/*		</li>)}*/}
					{/*	</ul>*/}
					{/*</li>*/}
					<ul>
						<li>
							<div>5 свежих записей</div>
						</li>
					</ul>
					<ul>
						<li>
							<div>Разделы нот</div>
						</li>
					</ul>
					<ul>
						<li>
							<div>Не нашли ноты которые ищете?</div>
						</li>
					</ul>
					<ul>
						<li>
							<div>Поддержите проект!</div>
						</li>
					</ul>
					<ul>
						<li>
							<div>Популярные ноты и минусы</div>
						</li>
					</ul>
				</ul>
			</div>
		</div>
	</div>
}