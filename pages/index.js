import Head from 'next/head';
import { useState, useEffect } from 'react';
import { useQuery } from 'react-query';
import axios from 'lib/axios';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
// import Slider from './../components/slider';

// import fetch from 'isomorphic-unfetch';

export async function getServerSideProps(context) {
	const { data } = await axios.get(`/api/notes`);
	return {
		props: { data },
	};
}

export default function Home({ data }) {
	const dispatch = useDispatch();
	
	return (
		<div>
			<Head>
				<title>Здесь будет тайтл</title>
				<link rel="icon" href="/favicon.ico" />
				<meta description="ЭТО САЙТ КОВАЛЬСКОГО!"/>
			</Head>

			<button type="button" className="main-content__link" onClick={() => {
				dispatch({
					type: 'set/song',
					payload: {
						song: `/audio/lj.mp3`,
						title: `MORGENSHTERN, ЭЛДЖЕЙ - Кадиллак (минус)`,
					},
				});
			}}>Проиграть
			</button>
		</div>
	)
}
