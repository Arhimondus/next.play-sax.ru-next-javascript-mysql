import React, { useEffect, useState } from 'react';
import './[note].module.scss';
import { useDispatch } from 'react-redux';
import axios from 'lib/axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Slider from '@farbenmeer/react-spring-slider';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Head from 'next/head';

function isContentOpened(url) {
	const urlParts = url.split('/');
	const content = urlParts.length === 4 ? decodeURI(urlParts.pop()) : null;
	return content;
}

export async function getServerSideProps({ req: { url }, res, query: { note } }) {
	const content = isContentOpened(url);

	const { data: contents } = await axios.get(`/api/note-contents/`, {
		params: { note },
	});

	const { data } = await axios.get(`/api/note-by-link/`, {
		params: { note },
	});

	console.log('[data]', data);
	console.log('content', content);

	return {
		props: { contents, note: data, content },
	};
}

function Note({ contents, note, content }) {
	const router = useRouter();
	const dispatch = useDispatch();

	const [currentContent, setCurrentContent] = useState(contents.find(it => it.url === content));

	useEffect(() => {
		dispatch({ type: 'set/hero', payload: 'main' });
		setCurrentContent(contents.find(it => it.url === content));
	}, [contents, content]);

	useEffect(() => {
		const handleRouteChange = (url, { shallow }) => {
			console.log(
				`App is changing to ${url} ${
					shallow ? 'with' : 'without'
				} shallow routing`
			);
			const content = isContentOpened(url);
			setCurrentContent(contents.find(it => it.url === content));
		}
	
		router.events.on('routeChangeStart', handleRouteChange)
	
		return () => {
			router.events.off('routeChangeStart', handleRouteChange)
		};
	}, []);

	return <div className="main-content">
		<Head>
			<title>{!currentContent ? note.seo_title : currentContent.seo_title}</title>
			<meta name="description" content={!currentContent ? note.seo_description : currentContent.seo_description}/>
		</Head>
		<div className="main-content__block">
			<nav className="main-content__nav">
				{!currentContent
					?	<ul className="tabs">
							<li className="tab --active">
								<FontAwesomeIcon icon={faInfoCircle} className=""/>
							</li>
							{contents.map(content =>
								<li key={content.id} className="tab">
									<a href={'/note' + content.full_url} className="tab__link" onClick={evt => {
										if(!evt.ctrlKey) {
											evt.preventDefault();
											setCurrentContent(content);
											router.push('/note' + content.full_url, null, { shallow: true });
										}
									}}>{content.title}</a>
								</li>
							)}
						</ul>
					:	<ul className="tabs">
							<li className="tab">
								<a href={'/note/' + currentContent.note_url} className="tab__link" onClick={evt => {
									if(!evt.ctrlKey) {
										evt.preventDefault();
										setCurrentContent(null);
										router.push('/note/' + note.url, null, { shallow: true });
									}
								}}><FontAwesomeIcon icon={faInfoCircle} className=""/></a>
							</li>
							{contents.map(content => <li key={content.id} className={["tab", content === currentContent ? '--active' : ''].join(' ')}>
								{content === currentContent ? content.title : <a href={'/note' + content.full_url} className="tab__link" onClick={evt => {
									if(!evt.ctrlKey) {
										evt.preventDefault();
										setCurrentContent(content);
										router.push('/note' + content.full_url, null, { shallow: true });
									}
								}}>{content.title}</a>}
							</li>)}
						</ul>
				}
			</nav>
			{!currentContent
				?	<>
						<h1>{note.composers} {note.title}</h1>
						<div className="main-content__content">
							{note.description}
						</div>
					</>
				:	<>
						<h1 className="main-content__header">{currentContent.composers} - {currentContent.note_title} ({currentContent.title})</h1>
						{currentContent.information && <p className="main-content__information">{currentContent.information}</p>}
						<div className="main-content__content">
							<div className="main-content__slider">
								{currentContent.pages_count > 0 &&
								<Slider
									activeIndex={0}
									hasBullets
									hasArrows={1}
									slidesAtOnce={2}
								>
									{new Array(currentContent.pages_count).fill(null).map((_, index) => {
										return <div key={index}><img
											src={`/contents/content${currentContent.id}-${index + 1}.jpg`}/></div>;
									})}
								</Slider>}
							</div>
							<ul>
								{currentContent.youtube_link &&
								<li>
									<a className="main-content__link" href={currentContent.youtube_link} target="_blank"
									   rel="noopener">Ссылка на ютуб</a>
								</li>}
								{currentContent.download_link &&
								<li>
									<a className="main-content__link" href={currentContent.download_link} target="_blank"
									   rel="noopener">Скачать</a>
								</li>}
								{currentContent.audio_file &&
								<li>
									<div className="main-content__link" onClick={() => {
										dispatch({
											type: 'set/song',
											payload: {
												song: `/audio/${currentContent.audio_file}`,
												title: `${currentContent.composers} - ${currentContent.note_title}`,
											},
										});
									}}>Проиграть
									</div>
								</li>}
							</ul>
						</div>
					</>
			}
		</div>
	</div>;
}

export default Note;