import { QueryClientProvider, QueryClient } from 'react-query'; 
import axios from 'axios';
import dynamic from 'next/dynamic';
import { Provider } from 'react-redux';

import './amplitude.css';
import 'components/main.scss';

import allReducers from '../redux';
import { createStore } from 'redux';

import Header from 'components/header';
import Nav from 'components/nav';
import Banner from 'components/banner';
import Footer from 'components/footer';

const store = createStore(allReducers);

// import AudioPlayer from 'components/audio-player';

const AudioPlayer = dynamic(() => import('components/audio-player'), {
	ssr: false,
});

// axios.defaults.baseURL = 'https://localhost:3000';

const queryClient = new QueryClient();

export default function PlaySaxApp({ Component, pageProps }) {
	return <QueryClientProvider client={queryClient}>
		<Provider store={store}>
			<Header/>
			<Nav/>
			<Banner/>

			<Component {...pageProps} />
			<div suppressHydrationWarning={true}>
			<AudioPlayer/>
			</div>

			<Footer/>
		</Provider>
	</QueryClientProvider>;
}