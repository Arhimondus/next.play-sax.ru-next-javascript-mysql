import React, { useEffect } from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
// import ContactForm from '../../ContactForm';
import Slider from '@farbenmeer/react-spring-slider';
import './[staticPage].module.scss';
import axios from 'lib/axios';
import { useRouter } from 'next/router'

// import FrescoFactory from '../../Slider/fresco';
import jquery from 'jquery';
// import '../../Slider/fresco.scss';

// const { Fresco, _Fresco } = FrescoFactory(jquery);
/// api/staticPage?link=lesson

export async function getServerSideProps({ req, res, query: { staticPage } }) {
	const { data } = await axios.get('/api/staticPage', { params: { link: staticPage } });
	return {
		props: { data },
	};
}

export default function StaticPage({ data: { title, content } }) {
	console.log(title, content);

	useEffect(() => {
		// _Fresco.initialize();
	}, []);

	function transform(node, index) {
		if (node.type !== 'tag') {
			return;
		}

		switch (node.name) {
			// case 'contactform':
			// 	return <ContactForm formId={node.attribs.formid}/>;
			case 'slider':
				return <Slider activeIndex={+node.attribs.activeindex}
							   hasBullets
							   hasArrows={+node.attribs.hasarrows}
							   slidesAtOnce={+node.attribs.slidesatonce} style={{ position: 'relative' }}>
					{node.children.filter(child => child.type === 'tag').map((child, index) => convertNodeToElement(child, index, transform))}
				</Slider>;
			default:
				return convertNodeToElement(node, index, transform);
		}
	}

	return <div>
		<h1>{title}</h1>
		<article>{ReactHtmlParser(content, { transform })}</article>
	</div>;
}