import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const query = 'SELECT * FROM genres';
		const [rows, fields] = await pool.query(query);
		res.json(rows);
	} catch (err) {
		res.status(400).send('Error!');
	}
}