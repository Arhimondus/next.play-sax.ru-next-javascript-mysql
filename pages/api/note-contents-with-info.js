import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const { note } = req.query;
		
		const args = [];
		let query = `SELECT 
			contents.id,
			contents.order_id,
			
			contents.title,
			contents.url AS url,					
			contents.information,
			
			contents.seo_title,
			contents.seo_description,
			
			contents.type,
			
			contents.pages_count,
	
			contents.download_link,
			contents.audio_file,
			contents.youtube_link,
			contents.youtube_embed,
			
			notes.id AS note_id,
			notes.url AS note_url,
			notes.title AS note_title,
			
			instruments.title as instrument,
			genres.title as genre,
			
			CONCAT('/', notes.url, '/', contents.url) AS full_url,
			GROUP_CONCAT(GetShortName(composers.imya, composers.familiya, composers.otchestvo) separator ', ') AS composers 
		FROM contents
			JOIN notes ON notes.id = contents.note_id
			JOIN notes_genres ON notes.id = notes_genres.note_id
			JOIN genres ON genres.id = notes_genres.genre_id
			LEFT JOIN content_instruments ON contents.id = content_instruments.content_id
			LEFT JOIN instruments ON content_instruments.instrument_id = instruments.id
			LEFT JOIN notes_composers ON notes.id = notes_composers.note_id
			LEFT JOIN content_levels ON notes.id = content_levels.content_id 
			LEFT JOIN composers ON notes_composers.composer_id = composers.id
			LEFT JOIN levels ON content_levels.level_id = levels.id
		WHERE notes.url = ? GROUP BY contents.id`;

		args.push(note);

		const [rows, fields] = await pool.query(query, args);
		res.json(rows);
	} catch (err) {
		res.status(400).send(err);
	}
}