import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const { genre, instrument } = req.query;
		const [[{ header }], fields] = await pool.query(`SELECT 
        concat(
            (SELECT title
            FROM genres 
            WHERE genres.url = ? LIMIT 1),
            " для ",
            (SELECT roditelniy
            FROM instruments 
            WHERE instruments.url = ? LIMIT 1)
        )
        AS header`, [genre, instrument]);

		res.json(header);
	} catch (err) {
		res.status(400).send(err);
	}
}