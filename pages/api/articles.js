import pool from 'lib/db';

export default async function handler(req, res) {
	const [rows, fields] = await pool.query('SELECT * FROM `articles`');
	res.json(rows);
}