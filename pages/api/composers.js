import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const { genre, instrument } = req.query;
		const [rows, fields] = await pool.query(`SELECT composers.id, GetShortName(composers.imya, composers.familiya, composers.otchestvo) AS name FROM composers
			JOIN notes_composers ON notes_composers.composer_id = composers.id
			JOIN notes ON notes_composers.note_id = notes.id
			JOIN contents ON contents.note_id = notes.id
			JOIN content_instruments ON content_instruments.content_id = contents.id
			JOIN instruments ON instruments.id = content_instruments.instrument_id
			JOIN notes_genres ON notes_genres.note_id = notes.id
			JOIN genres ON notes_genres.genre_id = genres.id
		WHERE genres.url = ? AND instruments.url = ?`, [genre, instrument]);
		res.json(rows);
	} catch (err) {
		res.status(400).send('Error!');
	}
}