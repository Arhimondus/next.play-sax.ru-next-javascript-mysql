import pool from 'lib/db';

export default async function handler(req, res) {
	const { link } = req.query;
	const [rows, fields] = await pool.query('SELECT * FROM `static_pages` WHERE link = ? LIMIT 1', [link]);
	res.json(rows[0]);
}