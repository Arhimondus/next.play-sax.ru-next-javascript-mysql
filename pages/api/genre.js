import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const { genre } = req.query;
		const args = [];
		let query = `SELECT 
			contents.id AS content_id,
			notes.id AS note_id,
			contents.order_id AS order_id,
			contents.type AS type,
			contents.title AS title,
			notes.title AS note_title,
			instruments.title as instrument,
			genres.title as genre,
			CONCAT('/', notes.url, '/', contents.url) AS url
		FROM contents
			JOIN notes ON notes.id = contents.note_id
			JOIN notes_genres ON notes.id = notes_genres.note_id
			JOIN genres ON genres.id = notes_genres.genre_id
			LEFT JOIN content_instruments ON contents.id = content_instruments.content_id
			LEFT JOIN instruments ON content_instruments.instrument_id = instruments.id
			LEFT JOIN content_levels ON notes.id = content_levels.content_id
			LEFT JOIN levels ON content_levels.level_id = levels.id
		WHERE genres.url = ?`;

		args.push(genre);

		query += ` GROUP BY contents.id`;
		let [rows, fields] = await pool.query(query, args);
		res.json(rows);
	} catch (err) {
		res.status(400).send(err);
	}
}