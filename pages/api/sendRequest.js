import pool from 'lib/db';
import sendMail  from 'lib/mail';

export default async function handler(req, res) {
	const message = {
		to: 'krylevsky.evgeny@yandex.ru',
		subject: `Письмо с сайта play-sax.ru от ${req.body.name}`,
		text: `
			Имя: ${req.body.name}, 
        	Телефон: ${req.body.phone},
        	E-mail: ${req.body.email},
        	Сообщение: ${req.body.message},
        	Идентификатор формы: ${req.body.formId}
        `,
	};
	sendEmail(message);
	res.send(`Спасибо за заявку, ${req.body.name}!`);
}