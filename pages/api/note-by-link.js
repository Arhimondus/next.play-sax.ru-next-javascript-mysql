import pool from 'lib/db';

export default async function handler(req, res) {
	const { note } = req.query;
	
	const query = `SELECT 
			notes.id,
			notes.title,
			notes.description,
			notes.url,
			notes.seo_title,
			notes.seo_description,
			
			genres.title as genre,
			GROUP_CONCAT(GetShortName(composers.imya, composers.familiya, composers.otchestvo) separator ', ') AS composers 
		FROM notes
			JOIN notes_genres ON notes.id = notes_genres.note_id
			JOIN genres ON genres.id = notes_genres.genre_id
			LEFT JOIN notes_composers ON notes.id = notes_composers.note_id
			LEFT JOIN composers ON notes_composers.composer_id = composers.id
		WHERE notes.url = ? LIMIT 1`;

	const [rows, fields] = await pool.query(query, [note]);
	res.json(rows[0]);
}