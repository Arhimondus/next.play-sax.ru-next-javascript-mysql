import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const { genre, instrument, pageNumber, pageSize } = req.query;
		const { composerIds, levelIds, sortType } = req.query;
		
		// console.log(genre, instrument, pageNumber, pageSize, composerIds, levelIds, sortType);

		const args = [];
		let query = `SELECT 
			contents.id AS content_id,
			notes.id AS note_id,
			contents.order_id AS order_id,
			contents.type AS type,
			contents.title AS title,
			notes.title AS note_title,
			instruments.title as instrument,
			genres.title as genre,
			CONCAT('/', notes.url, '/', contents.url) AS url,
			GROUP_CONCAT(GetShortName(composers.imya, composers.familiya, composers.otchestvo) separator ', ') AS composers 
		FROM contents
			JOIN notes ON notes.id = contents.note_id
			JOIN notes_genres ON notes.id = notes_genres.note_id
			JOIN genres ON genres.id = notes_genres.genre_id
			LEFT JOIN content_instruments ON contents.id = content_instruments.content_id
			LEFT JOIN instruments ON content_instruments.instrument_id = instruments.id
			LEFT JOIN notes_composers ON notes.id = notes_composers.note_id
			LEFT JOIN content_levels ON notes.id = content_levels.content_id 
			LEFT JOIN composers ON notes_composers.composer_id = composers.id
			LEFT JOIN levels ON content_levels.level_id = levels.id
		WHERE genres.url = ? AND instruments.url = ?`;

		args.push(genre);
		args.push(instrument);

		/* if(genre) {
			query += `genre.id IN (${genre})`;
		} */
		if (composerIds) {
			query += `AND composers.id IN (${composerIds.join(',')})`;
		}
		if (levelIds) {
			query += `AND levels.id IN (${levelIds.join(',')})`;
		}
		query += ` GROUP BY contents.id ORDER BY contents.order_id ASC`;
		switch (sortType) {
			case 'composer':
				query += `, composers.familiya ASC`;
				break;
			case 'note':
				query += `, notes.title ASC`;
				break;
			default:
				break;
		}
		query += ` LIMIT ? OFFSET ?`;
		args.push(+pageSize);
		args.push((+pageNumber - 1) * +pageSize);

		// console.log('query', query);

		let [rows, fields] = await pool.query(query, args);
		res.json(rows);
	} catch (err) {
		res.status(400).send(err);
	}
}