import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { faVk, faFacebookF, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Header() {
    return (
        <div className="header">
			<div className="column1">
                <a href="https://vk.com/ekrylevsky" target="_blank" rel="noreferrer">
                    <FontAwesomeIcon className="icons1" icon={faVk}/></a>
                <a href="https://www.facebook.com/krylevsky/">
                    <FontAwesomeIcon className="icons1" icon={faFacebookF}/></a>
                <a href="https://youtube.com/c/ЕвгенийКрылевский" target="_blank" rel="noreferrer">
                    <FontAwesomeIcon className="icons1" icon={faYoutube}/></a>
            </div>
            <div className="column2">
                <ul>
                    <li><a href="tel:+79055142527" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon className="icons3" icon={faPhoneAlt}/>89055142527</a></li>
                    <li><a href="mailto:info@play-sax.ru" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon className="icons2" icon={faEnvelope}/>info@play-sax.ru</a></li>
                </ul>
            </div>
        </div>
    );
}