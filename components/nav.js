import React from 'react';
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';

export default function Nav() {

	//нужно сделать меню третьего порядка. Где найти то видео с которого писалось меню?
	//доработать мобильное меню

	return (
		<div className="nav">
			<div>
				<div className="icon"><img src="/logo-nav.png"/></div>

				<div>
					<input type="checkbox" name="hrefggle" id="menuButhrefn" className="hrefggleButhrefn"/>
					<label for="menuButhrefn" className="hrefggleMenu">
						Меню
					</label>
					<div className="menu" id="mobileMenu">
						<ul>
							<li><Link href="/"><a>Главная</a></Link></li>
							<li><Link href="/training">Обучение</Link>
								<ul>
									<li><Link href="/lesson"><a>Уроки</a></Link></li>
									<li><Link href="/online-lesson"><a>Онлайн</a></Link></li>
									<li><Link href="/tuhrefrial"><a>Самоучитель</a></Link></li>
									<li><Link href="/scales"><a>Гаммы</a></Link></li>
									<li><Link href="/blog"><a>Статьи</a></Link></li>
									<li><Link href="/album"><a>Детский альбом</a></Link></li>
									<li><Link href="/video-lessons"><a>Видеоуроки</a></Link></li>
									<li><Link href="/gymnastics"><a>Гинмастика</a></Link></li>
									<li><Link href="/programs"><a>Программы</a></Link></li>
									<li><Link href="/useful-links"><a>Полезные ссылки</a></Link></li>
									<li><Link href="/encyclopedia"><a>Энциклопедия</a></Link></li>
								</ul>
							</li>
							<li><Link href="/notes"><a>Ноты</a></Link>
								<ul>
									<li><Link href=""><a>Популярные мелодии</a></Link>
										<ul>
											<li><Link href="/популярные-мелодии/альт"><a>Для альт-саксофона</a></Link></li>
										    <li><Link href="/популярные-мелодии/сопрано"><a>Для сопрано-саксофона</a></Link></li>
										</ul>
									</li>
									<li><Link href=""><a>Классические произведения</a></Link>
										<ul>
											<li><Link href="/классические-произведения/альт"><a>Для альт-саксофона</a></Link></li>
											<li><Link href="/классические-произведения/сопрано"><a>Для сопрано-саксофона</a></Link></li>
										</ul>
									</li>
									<li><Link href="/этюды"><a>Этюды</a></Link></li>
									<li><Link href="/ансамбли"><a>Ансамбли</a></Link></li>
									<li><Link href="/сборники"><a>Сборники</a></Link></li>
								</ul>
							</li>
							<li><Link href="/noty"><a>Творческая школа</a></Link>
								<ul>
									<li><Link href=""><a>Школа саксофона</a></Link>
										<ul>
											<li><Link href="/summer-school"><a>Летняя школа саксофона</a></Link></li>
											<li><Link href="/autumn-school"><a>Осенняя школа саксофона</a></Link></li>
											<li><Link href="/winter-school"><a>Зимняя школа саксофона</a></Link></li>
										</ul>
									</li>
									<li><Link href="/orchestra-school"><a>Оркестровая школа</a></Link></li>
									<li><Link href="/music-camp"><a>Летний музыкальный лагерь</a></Link></li>
									<li><Link href="/online-course"><a>Онлайн-курс по программированию</a></Link></li>
									<li><Link href="/summer-itcamp"><a>Летний лагерь прогарммирования</a></Link></li>
								</ul>
							</li>
							<li><Link href="/performance"><a>Выступления</a></Link></li>
							<li><Link href="/rent"><a>Аренда</a></Link></li>
							<li><Link href="/contacts"><a>Контакты</a></Link></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	)
}

