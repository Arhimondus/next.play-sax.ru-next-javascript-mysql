import React from 'react';
import { faAngleRight, faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { faVk, faFacebookF, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
// import { useParams } from 'react-router';
// import { useQuery } from 'react-query';
import axios from 'axios';

export default function Footer() {
	// const { isLoading: isLoading, data: articles } = useQuery(
	// 	'articles',
	// 	async () => (await axios.get(`http://localhost:3001/articles`)).data,
	// );

	// if (!articles || isLoading) return null;

	return (
		<div className="footer">
			<div>
				<div className="fcolumn1">
					<span>Страницы</span>
					<div><FontAwesomeIcon icon={faAngleRight} className="ficons"/><a href="www.yandex.ru">
						Аренда саксофона
					</a></div>
					<div><FontAwesomeIcon icon={faAngleRight} className="ficons"/><a href="www.yandex.ru">
						Летняя школа саксофона
					</a></div>
					<div><FontAwesomeIcon icon={faAngleRight} className="ficons"/><a href="www.yandex.ru">
						Уроки игры на саксофоне
					</a></div>
					<div><FontAwesomeIcon icon={faAngleRight} className="ficons"/><a href="www.yandex.ru">
						Обучения
					</a></div>
					<div><FontAwesomeIcon icon={faAngleRight} className="ficons"/><a href="www.yandex.ru">
						Контакты
					</a></div>
				</div>
				<div className="fcolumn2">
					<span>Свежие записи</span>
					{/* <ul style={{ margin: 0, padding: 0, listStyle: 'none' }}>{articles.map(article => <li
							key={`article-${article.id}`}>
							<FontAwesomeIcon icon={faAngleRight} className={"icons"}/><NavLink
							to={'/blog/' + article.link}>{article.title}</NavLink>
						</li>,
					)}</ul> */}
				</div>
				<div className="fcolumn3">
					<span>Всегда рады помочь Вам!</span>
					<div className="folumn5">
						<div className="fcolumn">
							<div><a href="www.yandex.ru"><FontAwesomeIcon icon={faPhoneAlt} className="ficons"/></a>
							</div>
							<div><a href="www.yandex.ru"><FontAwesomeIcon icon={faEnvelope} className="ficons"/></a>
							</div>
						</div>
						<div>
							<div><a href="tel:+79055142527">+7(905)514-25-27</a></div>
							<div><a href="mailto:info@play-sax.ru">info@play-sax.ru</a></div>
						</div>
					</div>
				</div>
				<div className="fcolumn4">
					<span>Пишите нам!</span>
					<div className="fcolumn5">
						<div className="fcolumn">
							<div><a href="https://www.facebook.com/krylevsky/"><FontAwesomeIcon icon={faFacebookF} className="ficons"/></a></div>
							<div><a href="https://vk.com/ekrylevsky"><FontAwesomeIcon icon={faVk} className="ficons"/></a></div>
							<div><a href="https://youtube.com/c/ЕвгенийКрылевский"><FontAwesomeIcon icon={faYoutube} className="ficons"/></a></div>
						</div>
						<div>
							<div><a href="https://www.facebook.com/krylevsky/">Facebook</a></div>
							<div><a href="https://vk.com/ekrylevsky">VK</a></div>
							<div><a href="https://youtube.com/c/ЕвгенийКрылевский">Youtube</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	)
}
