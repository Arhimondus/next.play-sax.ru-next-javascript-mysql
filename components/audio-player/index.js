import React, { useState, useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons';
import s from './audioPlayer.module.scss';
import { useDispatch, useSelector } from 'react-redux';

const Amplitude = require('amplitudejs');

export default function AudioPlayer() {
	const dispatch = useDispatch();
	const player = useSelector(state => state.player);
	const [playing, setPlaying] = useState(false);
	const playPauseButton = useRef(null);
	
	useEffect(() => {
		if (!player) {
			return;
		}
		
		Amplitude.init({
			continue_next: false,
			playback_speed: 1.0,
			songs: [{
				url: player.song,
			}],
		});

		playPauseButton.current.click();

		setInterval(() => {
			const playingState = playPauseButton?.current?.classList.contains('amplitude-playing');
			const pausedState = playPauseButton?.current?.classList.contains('amplitude-paused');
			if (playingState) {
				setPlaying(true);
			} else if (pausedState) {
				setPlaying(false);
			}
		}, 250);

		return () => {
			Amplitude.stop();
		}
	}, [player]);

	if (!player) {
		return null;
	}

	return <div className={s.saxPlayer}>
		{/*<Head>
			<link href="/components/audio-player/audioPlayer.module.scss" rel="stylesheet"/>
		</Head>*/}
		<div className={s.playerLeftBottom}>
			<div className={s.timeContainer} id="time-container">
				<div className={s.progressContainer} id="progress-container">
					<input type="range" className="amplitude-song-slider"/>
					<progress id="song-played-progress" className="song-played-progress" className="amplitude-song-played-progress"></progress>
					<progress id="song-buffered-progress" className="song-buffered-progress" className="amplitude-buffered-progress"
							  value="0"></progress>
				</div>
				<span className="current-time">
						<span className="amplitude-current-minutes"></span>:<span
					className="amplitude-current-seconds"></span>
				</span>&nbsp;/&nbsp;
				<span className="duration">
						<span className="amplitude-duration-minutes"></span>:<span
					className="amplitude-duration-seconds"></span>
				</span>
				<div className="volume-container" id="volume-container">
					<div className="volume-controls">
						<div className="amplitude-mute amplitude-not-muted"></div>
						<input type="range" className="amplitude-volume-slider"/>
						<div className="ms-range-fix"></div>
					</div>
					<div className="amplitude-shuffle amplitude-shuffle-off" className="shuffle-right"></div>
				</div>
				<span><button className={s.closePlayer} onClick={() => dispatch({ type: 'close/player' })}>X</button></span>
			</div>

			<div className="control-container" id="control-container">
				<div className="central-control-container" id="central-control-container">
					<div className={s.centralControls} id="central-controls">
						<div>{player.title}</div>
						<button className={s.amplitudePlayPause} ref={playPauseButton}>
							{playing ? <FontAwesomeIcon icon={faPause}/> : <FontAwesomeIcon icon={faPlay}/>}
						</button>
						<div>
							<button className={s.speedButton} onClick={() => Amplitude.setPlaybackSpeed(0.4)}>70%</button>
							<button className={s.speedButton} onClick={() => Amplitude.setPlaybackSpeed(0.6)}>80%</button>
							<button className={s.speedButton} onClick={() => Amplitude.setPlaybackSpeed(0.8)}>90%</button>
							<button className={s.speedButton} onClick={() => Amplitude.setPlaybackSpeed(1.0)}>ориг.</button>
							<button className={s.speedButton} onClick={() => Amplitude.setPlaybackSpeed(1.2)}>110%</button>
							<button className={s.speedButton} onClick={() => Amplitude.setPlaybackSpeed(1.4)}>120%</button>
						</div>
					</div>
				</div>
			</div>

			<div className="meta-container" id="meta-container">
				<span className="amplitude-playback-speed"></span>
				<span data-amplitude-song-info="name" className="song-name"></span>

				<div className="song-artist-album">
					<span data-amplitude-song-info="artist"></span>
					<span data-amplitude-song-info="album"></span>
				</div>
			</div>
		</div>
		
	</div>;
}