import mysql from 'mysql2/promise';

const pool = mysql.createPool({
	host: process.env.HOST,
	user: process.env.USER,
	database: process.env.DATABASE,
	password: process.env.PASSWORD,
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
});

export default pool;